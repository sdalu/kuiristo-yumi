# kuiristo-yumi
Ensemble de recettes pour Kuiristo


[![CC-BY-SA](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

```
Structure de repertoire:
recipe-name / recipe.xml
            / picture1.jpg
            / picture2.jpg
```         


Chosir l'id pour sa recette: `yyyymmddssss`
* yyyy: annee
* mm: mois
* dd: jour
* ssss: sequence

Les photos
* Priviliegier le format 16:9
* une resolution >= 1200x675
